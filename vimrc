syntax on
set number		" show line numbers
set noexpandtab 	" do not expand tabs regardless of tab's setting
set smartindent 	" set smartindent on
set autoindent 		" set autoindent on
set cindent 		" set cindent on
set mouse=a		" enable mouse support in console
set hlsearch 		" highlight search
set ignorecase		" ignore case when searching
set shiftwidth=4	" number of spaces to autoindent
set tabstop=4		" number of spaces of tab character

inoremap ( ()<Left>
inoremap [ []<Left>
inoremap { {}<Left>
